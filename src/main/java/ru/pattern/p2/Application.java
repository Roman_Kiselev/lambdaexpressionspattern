package ru.pattern.p2;

import java.util.function.Supplier;

/*
* lazy
*/

public class Application {
    public static void main(String[] args) {
        final int x = 4;
        if (x > 5 && compute(2) > 7) {
            System.out.println("result 1");
        } else {
            System.out.println("result 2");
        }

        final int temp1 = compute(2);
        if (x > 5 && temp1 > 7) {
            System.out.println("result 1");
        } else {
            System.out.println("result 2");
        }

        final Lazy<Integer> temp2 = new Lazy<>(() -> compute(2));
        if (x > 5 && temp2.getValue() > 7) {
            System.out.println("result 1");
        } else {
            System.out.println("result 2");
        }
    }

    private static int compute(int number) {
        System.out.println("called...");
        return number * number;
    }


}

class Lazy<T> {
    private final Supplier<T> supplier;
    public Lazy (Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public T getValue() {
        return supplier.get();
    }
}
