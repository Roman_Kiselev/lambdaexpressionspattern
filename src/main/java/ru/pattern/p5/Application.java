package ru.pattern.p5;

import java.util.function.Consumer;

public class Application {
    public static void main(String[] args) {
        Resource.use(resource -> resource.op1().op2());
    }
}

class Resource {
    public static void use(Consumer<Resource> consumer) {
        Resource resource = new Resource();
        try {
            consumer.accept(resource);
        } finally {
            resource.close();
        }
    }

    private void close() {
        System.out.println("...close...");
    }

    public Resource op1() {
        System.out.println("...op1...");
        return this;
    }

    public Resource op2() {
        System.out.println("...op2...");
        return this;
    }

}
