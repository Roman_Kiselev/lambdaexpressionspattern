package ru.pattern.p3;

import java.awt.*;
import java.util.function.*;
import java.util.stream.Stream;

/*
* pretty decorator
*/

public class Application {
    public static void print(Camera camera) {
        System.out.println(camera.snap(new Color(125, 125, 125)));
    }

    public static void main(String[] args) {
        print(new Camera());
        print(new Camera(Color::brighter));
        print(new Camera(Color::darker));
        print(new Camera(Color::darker, Color::brighter));
    }
}

class Camera {
    private Function<Color, Color> filter;
    public Camera(Function<Color, Color>... filters) {
        //filter = Stream.of(filters).reduce(Function.identity(), (result, aFilter) -> result.andThen(aFilter));
        filter = Stream.of(filters).reduce(Function.identity(), Function::andThen);

    }
    public Color snap(Color in) {
        return filter.apply(in);
    }
}
