package ru.pattern.p1;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


/*
* iterator
*/
public class Application {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        //System.out.println(totalAllValue(numbers));
        //System.out.println(totalEvenValue(numbers));
        //System.out.println(totalOddValue(numbers));
        System.out.println(totalValuers(numbers, e -> true));
        System.out.println(totalValuers(numbers, e -> e % 2 == 0));
        System.out.println(totalValuers(numbers, e -> e % 2 != 0));
    }

    private static int totalAllValue(List<Integer> numbers) {
        int result = 0;
        for (Integer i: numbers) {
            result += i;
        }
        return result;
    }

    private static int totalEvenValue(List<Integer> numbers) {
        int result = 0;
        for (Integer i: numbers) {
            if (i % 2 == 0) result += i;
        }
        return result;
    }

    private static int totalOddValue(List<Integer> numbers) {
        int result = 0;
        for (Integer i: numbers) {
            if (i % 2 != 0) result += i;
        }
        return result;
    }

    private static int totalValuers(List<Integer> numbers, Predicate<Integer> selector) {
        return numbers.stream().filter(selector).reduce(0, Integer::sum);
    }
}
