package ru.pattern.p4;

import java.util.function.Consumer;

public class Application {
    public static void main(String[] args) {
        Mailer.send(mailer ->
            mailer.from("test@gmail.com")
                .to("roman.kiselef@gmail.com")
                .subject("your code is sucks")
                .body("...details..."));
    }
}

class Mailer {
    private Mailer() {

    }
    public Mailer from(String addr){
        System.out.println("from " + addr);
        return this;
    }
    public Mailer to(String addr){
        System.out.println("to " + addr);
        return this;
    }
    public Mailer subject(String line){
        System.out.println("subject " + line);
        return this;
    }
    public Mailer body(String body){
        System.out.println("body " + body);
        return this;
    }
    public static void send(Consumer<Mailer> bloc){
        Mailer mailer = new Mailer();
        bloc.accept(mailer);
        System.out.println("...sending...");
    }
}
